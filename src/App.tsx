import './App.css';
import TodoPage from './page/TodoPage';

function App() {
  return (
    <div className="App">
      <TodoPage></TodoPage>
    </div>
  );
}

export default App;
