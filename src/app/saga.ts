
function* rootSaga() {
  const getTodo = localStorage.getItem('todos');
  if (getTodo) {
    const todos = JSON.parse(getTodo);
    console.log('todos localstorage: ', todos);
  }
}

export default rootSaga;