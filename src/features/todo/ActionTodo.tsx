import { format } from "date-fns";
import { Formik } from "formik";
import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { removeTodo, selectTodo, selectTodos, showDetail, TodoState, updateTodo } from "./todoSlice";

const ActionTodo = () => {
  const todos = useAppSelector(selectTodos);
  const dispatch = useAppDispatch();

  const handleChange = (payload: any) => {
    dispatch(selectTodo({ ...payload }));
  }

  const [searchText, setSearchText] = useState('');
  const [listTodoSearch, setListTodoSearch] = useState < Array < TodoState >> ([]);
  const handleSearch = (event: any) => {
    setSearchText(event.target.value);
    if (event.target.value.length > 0) {
      let searchTodo: Array<TodoState> = [];
      todos.forEach(item => {
        if (item.title.includes(event.target.value.trim())) {
          return searchTodo.push(item);
        }
      })
      setListTodoSearch(searchTodo);
    }
  }

  return (
    <>
      <div className="input-group">
        <h5 className="title-control">Search</h5>
        <input className="form-control" type="text" value={searchText} placeholder="Search..." onChange={handleSearch} />
      </div>
      {
        searchText.length > 0 ? (
          <>
            {listTodoSearch.length > 0 && (<>
              {listTodoSearch.map(item => (
                <Formik
                  key={item.id}
                  initialValues={{
                    title: item.title,
                    description: item.description,
                    due_date: item.due_date?.toString(),
                    priority: item.priority
                  }}
                  validate={values => {
                    const errors: any = {};
                    if (!values.title) {
                      errors.title = 'Required';
                    }
                    if (!values.due_date) {
                      values.due_date = format(new Date(), 'yyyy-MM-dd');
                    } else if (new Date(values.due_date).getTime() < new Date(format(new Date(), 'yyyy-MM-dd')).getTime()) {
                      errors.due_date = 'Do not accept days in the past as due date';
                    }
                    return errors;
                  }}
                  onSubmit={values => {
                    dispatch(updateTodo(values));
                  }}
                >
                  {formik => (
                    <div className="item-todo">
                      <div className="card">
                        <div className="card-header">
                          <div className="form-check">
                            <input type="checkbox" onChange={() => handleChange({ id: item.id })} />
                          </div>
                          <div className="content-title">
                            <p>
                              {item.title}
                            </p>
                          </div>
                          <div>
                            <div className="group-btn">
                              <button className="btn btn-small btn-info" type="button" onClick={() => dispatch(showDetail({ id: item.id }))}>Detail</button>
                              <button className="btn btn-small btn-danger" type="button" onClick={() => dispatch(removeTodo({ id: item.id }))}>Remove</button>
                            </div>
                          </div>
                        </div>
                        {
                          item.isDetail &&
                          (
                            <div className="card-body">
                              <form action="#!" onSubmit={formik.handleSubmit}>
                                <div className="input-group">
                                  <h5 className="title-control">Title</h5>
                                  <input className="form-control"
                                    name="title"
                                    id="title"
                                    type="text" placeholder="Add new task..."
                                    onChange={formik.handleChange}
                                    value={formik.values.title}
                                  />
                                  {formik.errors.title ? <div className="error-danger">{formik.errors.title}</div> : null}
                                </div>
                                <div className="input-group">
                                  <h5 className="title-control">Description</h5>
                                  <textarea className="form-control"
                                    id="description"
                                    name="description"
                                    rows={5}
                                    onChange={formik.handleChange}
                                    value={formik.values.description}
                                  ></textarea>
                                </div>
                                <div className="flex-form">
                                  <div className="input-group flex-col">
                                    <h5 className="title-control">Due Date</h5>
                                    <input className="form-date" type="date"
                                      id="due_date"
                                      name="due_date"
                                      onChange={formik.handleChange}
                                      value={formik.values.due_date}
                                    />
                                    {formik.errors.due_date ? <div className="error-danger">{formik.errors.due_date}</div> : null}
                                  </div>
                                  <div className="input-group flex-col">
                                    <h5 className="title-control">Priority</h5>
                                    <select className="form-select"
                                      id="priority"
                                      name="priority"
                                      onChange={formik.handleChange}
                                      value={formik.values.priority}
                                    >
                                      <option value="low">low</option>
                                      <option value="normal">normal</option>
                                      <option value="high">high</option>
                                    </select>
                                  </div>
                                </div>
                                <button className="btn btn-full btn-green" type="submit">Update</button>
                              </form>
                            </div>
                          )
                        }
                      </div>
                    </div>
                  )}
                </Formik>
              ))}
            </>)}
          </>
        ) : (
          <>
            {
              todos.length > 0 && (
                <>
                  {todos.map(item => (
                    <Formik
                      key={item.id}
                      initialValues={{
                        title: item.title,
                        description: item.description,
                        due_date: item.due_date?.toString(),
                        priority: item.priority
                      }}
                      validate={values => {
                        const errors: any = {};
                        if (!values.title) {
                          errors.title = 'Required';
                        }
                        if (!values.due_date) {
                          values.due_date = format(new Date(), 'yyyy-MM-dd');
                        } else if (new Date(values.due_date).getTime() < new Date(format(new Date(), 'yyyy-MM-dd')).getTime()) {
                          errors.due_date = 'Do not accept days in the past as due date';
                        }
                        return errors;
                      }}
                      onSubmit={values => {
                        dispatch(updateTodo(values));
                      }}
                    >
                      {formik => (
                        <div className="item-todo">
                          <div className="card">
                            <div className="card-header">
                              <div className="form-check">
                                <input type="checkbox" onChange={() => handleChange({ id: item.id })} />
                              </div>
                              <div className="content-title">
                                <p>
                                  {item.title}
                                </p>
                              </div>
                              <div>
                                <div className="group-btn">
                                  <button className="btn btn-small btn-info" type="button" onClick={() => dispatch(showDetail({ id: item.id }))}>Detail</button>
                                  <button className="btn btn-small btn-danger" type="button" onClick={() => dispatch(removeTodo({ id: item.id }))}>Remove</button>
                                </div>
                              </div>
                            </div>
                            {
                              item.isDetail &&
                              (
                                <div className="card-body">
                                  <form action="#!" onSubmit={formik.handleSubmit}>
                                    <div className="input-group">
                                      <h5 className="title-control">Title</h5>
                                      <input className="form-control"
                                        name="title"
                                        id="title"
                                        type="text" placeholder="Add new task..."
                                        onChange={formik.handleChange}
                                        value={formik.values.title}
                                      />
                                      {formik.errors.title ? <div className="error-danger">{formik.errors.title}</div> : null}
                                    </div>
                                    <div className="input-group">
                                      <h5 className="title-control">Description</h5>
                                      <textarea className="form-control"
                                        id="description"
                                        name="description"
                                        rows={5}
                                        onChange={formik.handleChange}
                                        value={formik.values.description}
                                      ></textarea>
                                    </div>
                                    <div className="flex-form">
                                      <div className="input-group flex-col">
                                        <h5 className="title-control">Due Date</h5>
                                        <input className="form-date" type="date"
                                          id="due_date"
                                          name="due_date"
                                          onChange={formik.handleChange}
                                          value={formik.values.due_date}
                                        />
                                        {formik.errors.due_date ? <div className="error-danger">{formik.errors.due_date}</div> : null}
                                      </div>
                                      <div className="input-group flex-col">
                                        <h5 className="title-control">Priority</h5>
                                        <select className="form-select"
                                          id="priority"
                                          name="priority"
                                          onChange={formik.handleChange}
                                          value={formik.values.priority}
                                        >
                                          <option value="low">low</option>
                                          <option value="normal">normal</option>
                                          <option value="high">high</option>
                                        </select>
                                      </div>
                                    </div>
                                    <button className="btn btn-full btn-green" type="submit">Update</button>
                                  </form>
                                </div>
                              )
                            }
                          </div>
                        </div>
                      )}
                    </Formik>
                  ))}
                </>
              )
            }
          </>
        )
      }

    </>
  )
}

export default ActionTodo;