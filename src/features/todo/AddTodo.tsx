import { useFormik } from "formik";
import { format } from "date-fns";
import { useAppDispatch } from "../../app/hooks";
import { addTodo } from "./todoSlice";
import { useState } from "react";

const AddTodo = () => {
  const dispatch = useAppDispatch();
  const [isSubmit, setIsSubmit] = useState(false);
  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      due_date: format(new Date(), 'yyyy-MM-dd'),
      priority: 'normal'
    },
    validate: values => {
      console.log('values.due_date: ', new Date(values.due_date).getTime());
      console.log('now: ', new Date(format(new Date(), 'yyyy-MM-dd')).getTime());
      const errors: any = {};
      if (!values.title) {
        errors.title = 'Required';
      }
      if (!values.due_date) {
        values.due_date = format(new Date(), 'yyyy-MM-dd');
      } else if (new Date(values.due_date).getTime() < new Date(format(new Date(), 'yyyy-MM-dd')).getTime()) {
        errors.due_date = 'Do not accept days in the past as due date';
      }
      return errors;
    },
    onSubmit: values => {
      let payload = { ...values }
      dispatch(addTodo(payload));
      values.title = '';
      values.description = '';
      values.due_date = format(new Date(), 'yyyy-MM-dd');
      values.priority = 'normal';
      setIsSubmit(submit => submit = !isSubmit);
    }
  })

  return (
    <form action="#!" onSubmit={formik.handleSubmit}>
      <div className="input-group">
        <h5 className="title-control">Title</h5>
        <input className="form-control"
          name="title"
          id="title"
          type="text" placeholder="Add new task..."
          onChange={formik.handleChange}
          value={formik.values.title}
        />
        {formik.errors.title ? <div className="error-danger">{formik.errors.title}</div> : null}
      </div>
      <div className="input-group">
        <h5 className="title-control">Description</h5>
        <textarea className="form-control"
          id="description"
          name="description"
          rows={5}
          onChange={formik.handleChange}
          value={formik.values.description}
        ></textarea>
      </div>
      <div className="flex-form">
        <div className="input-group flex-col">
          <h5 className="title-control">Due Date</h5>
          <input className="form-date" type="date"
            id="due_date"
            name="due_date"
            onChange={formik.handleChange}
            value={formik.values.due_date}
          />
          {formik.errors.due_date ? <div className="error-danger">{formik.errors.due_date}</div> : null}
        </div>
        <div className="input-group flex-col">
          <h5 className="title-control">Priority</h5>
          <select className="form-select"
            id="priority"
            name="priority"
            onChange={formik.handleChange}
            value={formik.values.priority}
          >
            <option value="low">low</option>
            <option value="normal">normal</option>
            <option value="high">high</option>
          </select>
        </div>
      </div>
      <button className="btn btn-full btn-green" type="submit">Add</button>
    </form>
  )
}

export default AddTodo;