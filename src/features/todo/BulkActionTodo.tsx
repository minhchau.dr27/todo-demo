import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { removeTodos, selectTodos } from "./todoSlice";

const BulkActionTodo = () => {
  const todos = useAppSelector(selectTodos);
  const dispatch = useAppDispatch();
  return (
    <>
      {todos.some(f => f.checked) && (
        <div className="side-bulk">
          <div>
            Bulk Action
          </div>
          <div className="group-btn">
            <button className="btn btn-primary" type="button">Done</button>
            <button className="btn btn-danger" type="button" onClick={() => dispatch(removeTodos())}>Remove</button>
          </div>
        </div>
      )}
    </>
  )
}

export default BulkActionTodo;