import { RootState } from './../../app/store';
import { createSlice, nanoid, PayloadAction, PrepareAction } from '@reduxjs/toolkit';

export interface TodoState {
  id: any;
  title: string;
  description?: string;
  due_date?: Date | string;
  priority?: 'low' | 'normal' | 'high';
  checked?: boolean;
  isDetail?: boolean;
}

const initialState: Array<TodoState> = [];

export const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    addTodo: {
      reducer: (state, action: PayloadAction<TodoState>) => {
        state.push(action.payload);
        state.sort((a, b) => {
          return new Date(a?.due_date as Date).getTime() - new Date(b.due_date as Date).getTime()
        });
      },
      prepare: (state) => {
        state.id = nanoid();
        state.checked = false;
        state.isDetail = false;
        return {
          payload: {
            ...state
          }
        }
      }
    },
    selectTodo: (state, action: PayloadAction<any>) => {
      return state.map(m => m.id === action.payload.id ? { ...m, checked: !m.checked } : m);
    },
    updateTodo: (state, action: PayloadAction<any>) => {
      state.map(m => m.id === action.payload.id ? { ...action.payload } : m);
      let sortState = state.sort((a, b) => {
        return new Date(a?.due_date as Date).getTime() - new Date(b.due_date as Date).getTime()
      });
      return sortState
    },
    removeTodo: (state, action: PayloadAction<any>) => {
      let filterState = state.filter(f => f.id !== action.payload.id);
      return filterState;
    },
    removeTodos: (state) => {
      let newState: Array<TodoState> = [];
      state.forEach(item => {
        if (!item.checked) return newState.push({ ...item });
      });
      return newState;
    },
    showDetail: (state, action: PayloadAction<any>) => {
      return state.map(m => m.id === action.payload.id ? { ...m, isDetail: !m.isDetail } : m);
    },
    fetchTodo: (state, action: PayloadAction<[TodoState]>) => {
      return action.payload;
    }
  },
  extraReducers: {}
});

export const { addTodo, selectTodo, updateTodo, removeTodo, removeTodos, showDetail, fetchTodo } = todoSlice.actions;

export const selectTodos = (state: RootState) => state.todo;

export default todoSlice.reducer;