import ActionTodo from "../features/todo/ActionTodo";
import AddTodo from "../features/todo/AddTodo";
import BulkActionTodo from "../features/todo/BulkActionTodo";

const TodoPage = () => {
  return (
    <div className="box-todo">
      <div className="col-todo col-add">
        <div className="block add-todo">
          <div className="block-header">
            <h4 className="block-title">New Task</h4>
          </div>
          <div className="block-body">
            <AddTodo></AddTodo>
          </div>
        </div>
      </div>
      <div className="col-todo col-list">
        <div className="block list-doto">
          <div className="block-header">
            <h4 className="block-title">To Do List</h4>
          </div>
          <div className="block-body">
            <ActionTodo></ActionTodo>
          </div>
        </div>
        <BulkActionTodo></BulkActionTodo>
      </div>
    </div>
  )
}

export default TodoPage;